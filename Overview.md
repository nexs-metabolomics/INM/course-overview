---
title: "Introduction to Nutritional Metabolomics"
output: 
  html_document: 
    css: custom.css
---


## Course material


**Legend**

* ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) Missing
* ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Freely available
* ![#1589F0](https://via.placeholder.com/15/1589F0/000000?text=+) Password protected
* <img class="pres"> Presentation
* <img class="book"> Written material for *preparation*

<div class="day">
### Before the course

Because we will use R heavily in the course and most of our students have not used R before we ask you to go through a [collection of videos and short texts](https://nexs-metabolomics.gitlab.io/INM/r-intro-material/R_intro_material.html) before the course. We do not expect you to come with the ability to code on your own; we will help you through your first steps in the course. But having an overview before the course will save time.

Please also attempt to install the programs listed [here](https://nexs-metabolomics.gitlab.io/INM/coffee-walkthrough/01_installing_software.html).

</div>


<div class="day">
### Day 1

* Outline of the course and introduction of participants: <img class="pres missing">&nbsp;<img class="book missing">
* Perspectives and goals of metabolomics: <img class="pres missing">&nbsp;<img class="book missing">
* Metabolomics in Nutrition: <img class="pres missing">&nbsp;<img class="book missing">
* Overview of the metabolomics pipeline: From sample to data interpretation: <img class="pres missing">&nbsp;<img class="book missing">
* Experimental design: <img class="pres missing">&nbsp;<img class="book missing">
* LC-MS based metabolomics
  * Subject 1: Samples and sample preparation in metabolomics: <img class="pres missing">&nbsp;<img class="book missing">
  * Subject 2: Liquid chromatography and mass spectrometry principles: <img class="pres missing">&nbsp;<img class="book missing">
  * Subject 3:  LC-MS technologies for targeted and untargeted metabolomics: <img class="pres missing">&nbsp;<img class="book missing">
* Navigating freely-available software tools for metabolomics analysis: <img class="pres missing">&nbsp;<img class="book missing">

</div>

<div class="day">
### Day 2

* Data preprocesssing in metabolomics: Basic concepts in preprocessing (e.g. feature detection, alignment, gap filling): <img class="pres missing">&nbsp;<img class="book missing">
* Conversion of raw data: <a href="https://gitlab.com/nexs-metabolomics/INM/conversion-of-raw-data/-/raw/master/data_conversion.pptx"><img class="pres free"></a>&nbsp;<img class="book none">
* A brief introduction to R: Basic syntax, working in Rstudio: <a href="https://nexs-metabolomics.gitlab.io/INM/r-intro-material/intro_to_R.html"><img class="pres free"></a>&nbsp;<a href="https://nexs-metabolomics.gitlab.io/INM/r-intro-material/R_intro_material.html"><img class="book free"></a>
* A brief introduction to R: We import data together, modify it, and export: <img class="pres missing">&nbsp;<img class="book missing">
* XCMS walk-through and CAMERA annotation: <img class="pres missing">&nbsp;<img class="book missing">
* Presentation of exercise LC-MS dataset: Extraction of food intake markers: <img class="pres missing">&nbsp;<img class="book missing">
* Exercise: Hands on LC-MS data preprocessing (XCMS): <img class="pres missing">&nbsp;<img class="book missing">&nbsp;<img class="ex missing">
* Exercise: Hands on LC-MS data preprocessing (XCMS): <img class="pres missing">&nbsp;<img class="book missing">&nbsp;<img class="ex missing">
* Introduction to normalization and transformations: <img class="pres missing">&nbsp;<img class="book missing">
* Exercise: Continued exercise: <img class="pres missing">&nbsp;<img class="book missing">&nbsp;<img class="ex missing">

</div>


<div class="day">
### Day 3

* Data Analysis I: Univariate Methods: <img class="pres missing">&nbsp;<img class="book missing">
* Exercise: Hands on univariate analysis using R: <img class="pres missing">&nbsp;<img class="book missing">
* Data Analysis II: Multivariate Approach - PCA: <img class="pres missing">&nbsp;<img class="book missing">
* Exercise: Hands on PCA using R: <img class="pres missing">&nbsp;<img class="book missing">&nbsp;<img class="ex missing">
* Exercise: Hands on PCA using R: <img class="pres missing">&nbsp;<img class="book missing">&nbsp;<img class="ex missing">
* Data Analysis III: Multivariate Approach - PLSDA: <img class="pres missing">&nbsp;<img class="book missing">
* Exercise: Hands on PLSDA using R: <img class="pres missing">&nbsp;<img class="book missing">&nbsp;<img class="ex missing">

</div>



<div class="day">
### Day 4

* Metabolite Identification: <img class="pres missing">&nbsp;<img class="book missing">
* Exercise: Fragmentation, adducts, annotation: <img class="pres missing">&nbsp;<img class="book missing">
* Compound and spectral databases, in-silico prediction, levels of identification: <img class="pres missing">&nbsp;<img class="book missing">
* Exercise: Fragmentation, adducts, annotation: <img class="pres missing">&nbsp;<img class="book missing">&nbsp;<img class="ex missing">
* Exercise: Hands on - Identification of markers of food intake: <img class="pres missing">&nbsp;<img class="book missing">&nbsp;<img class="ex missing">

</div>

## Project walkthrough

## Forms
* Dinner


## Evaluation
